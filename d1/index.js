console.log("Hello World!")

// While loop

let count = 5;

while(count !== 0){
    console.log("While: "+ count)
    count-- 
}

// Do-While Loop
// do{statement}
// while(expression/condition)

let number = Number(prompt("Give me a number"));

do{
    console.log("Do While: "+ number);
    number += 1;
}while(number <= 10);

//  For loops

// for (let count = 0; count <= 20; count++){
//     console.log(count)
// }

// for (let count = 0; count <= 28; count++){
//     console.log(count)
// }

let myString = "Alex";
console.log(myString.length);

console.log(myString[0])
console.log(myString[1])
console.log(myString[2])

// console.log(myString[-1])

for(let x = 0; x < myString.length; x++ ){
    console.log(myString[x]);
}

// Create aa string myName
let myName = "Lawrence";

for(let i = 0; i < myName.length; i++){
    // console.log(myName[i].toLowerCase());

    if(
        myName[i].toLowerCase() == "a" || 
        myName[i].toLowerCase() == "i" || 
        myName[i].toLowerCase() == "e" || 
        myName[i].toLowerCase() == "o" || 
        myName[i].toLowerCase() == "u" 
    ){
        console.log(3)
    }else{
        console.log(myName[i])
    }
}

// Continue and Break statement

for (let count = 0; count <= 20; count++){
    if (count % 2 === 0){
        continue;
    }
    console.log("Continue and break: " + count);
    if (count > 10){
        break;
    }

}

let name = "alexandro";
for (let i = 0; i < name.length; i++){
    console.log(name[i]);

    if(name[i].toLowerCase()){
        console.log("Continue to next iteration");
    }
    if(name[i] == "d"){
        break;
    }
}
